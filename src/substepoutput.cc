
#include "math.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"

#include "cctk.h" 
#include "cctk_Arguments.h" 
#include "cctk_Parameters.h" 

#include "util_Table.h"

#define DEBUG
#include "assert.h"

static int mol_substep(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS

  CCTK_INT *substep_ptr = (CCTK_INT *) CCTK_VarDataPtr(cctkGH, 0, "MoL::MoL_Intermediate_Step");
  if (substep_ptr == NULL)
  {
    CCTK_WARN (0, "Could not read variable MoL::MoL_Intermediate_Step");
  }
  CCTK_INT substep = *substep_ptr;

  CCTK_INT *substeps_ptr = 
    (CCTK_INT *) CCTK_ParameterGet("MoL_Intermediate_Steps", "MoL", NULL); 
  if (substeps_ptr == NULL)
  {
    CCTK_WARN (0, "Could not read parameter MethodOfLines::MoL_Intermediate_Steps");
  }
  CCTK_INT substeps = *substeps_ptr;

  return substeps - substep;
}


static void output_var(int const varindex, char const * const optstring, void * const arg)
{
  DECLARE_CCTK_PARAMETERS

  cGH *cctkGH = (cGH *) arg;
  
  assert (varindex >= 0);
  
  char * const fullvarname = CCTK_FullName (varindex);
  assert (fullvarname);

  const char * const varname = CCTK_VarName (varindex);
  assert (varname);

  char alias_name[strlen(varname) + 10];
  int substep = mol_substep(CCTK_PASS_CTOC);

  int chars_written = snprintf(alias_name, sizeof(alias_name), "%s_%d", varname, substep);
  assert(chars_written >= 0 && chars_written < sizeof(alias_name));

  CCTK_VInfo (CCTK_THORNSTRING,
                "Outputting \"%s\" on MoL substep %d as \"%s\"",
              fullvarname, substep, alias_name);
  CCTK_OutputVarAsByMethod(cctkGH, fullvarname, output_method, alias_name);

  // This outputs using all methods, which is probably not what you want
  // CCTK_OutputVarAs(cctkGH, fullvarname, alias_name);

  free(fullvarname);
}

extern "C" void SubstepOutput_Output(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS

  CCTK_TraverseString(vars, output_var, cctkGH, CCTK_GROUP_OR_VAR);

  return;
}


